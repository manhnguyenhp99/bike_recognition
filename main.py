from flask import Flask
from flask import jsonify
from tensorflow.keras.applications.vgg19 import preprocess_input
import tensorflow as tf
from tensorflow import keras
import numpy as np
import pandas as pd
app = Flask(__name__)

def get_image(img_path):
    try:
        image = tf.io.read_file(img_path)
        image = tf.io.decode_jpeg(image, channels=3)
        image = tf.image.resize(image, [224, 224])
        image = preprocess_input(image)
        image = np.expand_dims(image, axis=0)
        return image
    except:
        return None

@app.route('/image/<string:name>')
def image(name):
    img_path = '/home/fancol/Downloads/'+ name
    if get_image(img_path) is not None:
        image = get_image(img_path)

        spe_model = keras.models.load_model('weight/specialized.h5')
        trek_model = keras.models.load_model('weight/trek.h5')
        giant_model = keras.models.load_model('weight/giant.h5')
        type_model = keras.models.load_model('weight/type.h5')

        spe_pred = spe_model.predict(image)
        trek_pred = trek_model.predict(image)
        giant_pred = giant_model.predict(image)
        type_pred = type_model.predict(image)

        data = pd.read_csv('./database_label/model.csv')
        spe_classes = data[data['brand_id'] == 1]['model'].tolist()
        trek_classes = data[data['brand_id'] == 2]['model'].tolist()
        giant_classes = data[data['brand_id'] == 4]['model'].tolist()

        data = pd.read_csv('./database_label/type.csv')
        type_classes = data['type'].tolist()


        pres = [np.max(spe_pred), np.max(trek_pred), np.max(giant_pred)]

        if np.argmax(pres) == 0:
            return jsonify(
                model=spe_classes[np.argmax(spe_pred)],
                brand="Specialized",
                accuracy=str(np.max(spe_pred)),
                type=str(type_classes[np.argmax(type_pred)])
            )
        elif np.argmax(pres) == 1:
            return jsonify(
                model=trek_classes[np.argmax(trek_pred)],
                brand="Trek",
                accuracy=str(np.max(trek_pred)),
                type=str(type_classes[np.argmax(type_pred)])
            )
        
        elif np.argmax(pres) == 2:
            return jsonify(
                model=giant_classes[np.argmax(giant_pred)],
                brand="Giant",
                accuracy=str(np.max(giant_pred)),
                type=str(type_classes[np.argmax(type_pred)])
            )
    else:
        return "url not found"

if __name__ == '__main__':
  app.run(host='0.0.0.0', port=5001)