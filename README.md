# BIKE RECOGNITION
This is a project for recognition bikes

Author: **Trần Tuấn Ngọc** ([Fancol Tran](https://gitlab.com/ngoc_98))
## PYTHON VERSION
**3.8**
## DATABASE STRUCTURE
Database Management System: Postgresql 12  <br>
Database name: bike <br>
Database diagram:  <br>

![database_diagram](database_diagram.png)

## INSTALL
```
git clone https://gitlab.com/ngoc_98/bike_recognition.git 
```

## INSTALL POSTGRESQL(UBUNTU)
Ubuntu 20.04: <br>
```
sudo apt-get install postgresql-12
```
other ubuntu versions: [see here](https://www.postgresql.org/download/linux/ubuntu/)

## RUNNING POSTGRESQL
```
sudo systemctl start postgresql.service
```
## SETTUP
```
pip install -r requirements.txt
```
### RUNNING
if your python executable is python
```
python main.py
```
if your python executable is python3
```
python3 main.py
```

## POSTGRESQL GUIDE
> Some of the guides on the internet may confuse you with different ways. This will explain all of it<br>

The installation procedure created a user account called postgres on your linux system. This is the admin role of postgresql <br>
1. To go to the postgresql shell: You have two ways: <br>
* Switch over to the postgres account on your linux
```
sudo -i -u postgres
```
if you want to exit postgres account, type:
```
exit
```
after go to postgres account, run psql shell:
```
psql
```

* Run the psql command as the postgres account directly
```
sudo -u postgres psql
```
To exit out of postgresql shell, type \q: 
```php
postgres=# ("\q");
```
